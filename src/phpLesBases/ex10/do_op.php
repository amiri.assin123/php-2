<?php
const INVALID_PARAMETERS = 'Incorrect Parameters' . "\n";
// Verification qu'il y a exactement 3 arguments
if ($argc !== 4) {
    echo INVALID_PARAMETERS;
    exit();
}

// Verification que les arguments ne sont pas null
if (isset($argv[1], $argv[2], $argv[3])) {
    // Verification que les arguments 1 et 3 sont bien des nombres
    if (!is_numeric($argv[1]) || !is_numeric($argv[3])) {
        echo INVALID_PARAMETERS;
        exit();
    }
    // création d'une variable pour chaque argument + supression des espaces
    $var1 = trim($argv[1]);
    $operation = trim($argv[2]);
    $var2 = trim($argv[3]);

    // Opération mathématique en fonction de l'opérateur $operation
    switch ($operation) {
        case '+':
            echo $var1 + $var2 . "\n";
            break;

        case '-':
            echo $var1 - $var2 . "\n";
            break;

        case '*':
            echo $var1*$var2 . "\n";
            break;

        case '/':
            // if ($var2 == 0) {
            //     echo '0' . "\n";
            // } else {
            //     echo $var1 / $var2 . "\n";
            // }
            echo ($var2 == 0 ? '0' : $var1 / $var2) . "\n";
            break;

        case '%':
            echo $var1 % $var2 . "\n";
            break;

        default:
            echo INVALID_PARAMETERS;
            break;
    }
} else {
    echo INVALID_PARAMETERS;
    exit();
}