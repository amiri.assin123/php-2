#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

if [ "$1" = 'php-fpm' ] || [ "$1" = 'php' ]; then
	PHP_INI_RECOMMENDED="$PHP_INI_DIR/php.ini-production"

	ln -sf "$PHP_INI_RECOMMENDED" "$PHP_INI_DIR/php.ini"

	composer install --prefer-dist --no-progress --no-interaction
fi

exec docker-php-entrypoint "$@"
